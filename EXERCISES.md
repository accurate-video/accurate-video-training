# Accurate.Video Validate configuration exercises <!-- omit in toc -->

This document outlines some basic exercises for Accurate.Video Validate configuration.

The exercises are divided into the following areas:

- [Marker configuration](#marker-configuration)
- [Metadata views and fields](#metadata-views-and-fields)
- [Forms configuration](#forms-configuration)
- [Marker table configuration](#marker-table-configuration)
- [Other settings](#other-settings)

## Marker configuration
1. Creating a marker group
   - Create a marker group to be displayed in timeline.
   - The group should always be visible.
   - The group should allow you to create new tracks.
2. Creating a marker row
   - Configure two rows in the marker group.
   - One should be **read only**.
   - The color of the markers of the rows should be different.
   - The tooltip on marker hover should be **description** for one row and **name** for another.
3. Generating marker groups and rows from data
   - Configure a group and rows to match the timespans in the **timespans_generated.json** file.
   - The group should not be visible if no timespans match that group
   - You should be allowed to create new tracks in this group
   - Markers with row **“First”** metadata should be shown in one row and markers with row **“Second”** should be shown in another row.
   - Run `./import_timespans.py fake_data/timespans_generated.json <assetId>` to load timespans

## Metadata views and fields

1. Creating metadata fields
   - Create two new fields to display the metadata **director** and **release_date** on the asset.
2. Extend the `assetType` metadata field to display expected value from metadata key `asset_type_expected` with the comparison from metadata key `asset_type_expected_result`.
3. Creating metadata views
   - Create a new metadata view. The view should contain the metadata fields created earlier as read only fields for the asset fieldset as well as the `assetType` metadata field with its expected metadata.
4. Enabling and disabling views
   - Disable the default metadata view and enable the newly created metadata view.

## Forms configuration

1. Overwriting default forms
   - Overwrite default marker form with a new form.
2. Creating a new form
   - At least one **Group**
   - At least one **Horizontal layout**
   - At least one **Vertical layout**
   - At least one **Conditional field**
   - At least one **Reference field** using the **http://localhost/static/actors-field.json** file.
3. Adding forms to marker creation
   - Add the new form to a marker row.
4. Adding forms to metadata fieldset
   - Add the new form to the asset metadata fieldset

## Marker table configuration

1. Add columns to marker table
   - Overwrite the marker table definitions to match the marker form previously created.
2. Create a composite column
   - Create a composite column, displaying a combination of at least two metadata values.
   - Map the value to another value using an if statement.
   
## Other settings

1. File display order
   - Reorder the video files by language.
   - Rename the video files to display the asset name instead of filename.
   - Reorder the audio files by tag.
   - Rename the audio files to not include the file id.

2. Asset status
   - Add a new asset status.
   - Add a form to the new asset status.
   - Disable the asset status feature.

3. Enabled workspaces
   - Disable the ad break workspace
4. Waveforms
   - Change the min and max sample height of the waveforms in the timeline.
5. Languages
   - Overwrite the default languages with some additional languages.
5. Video & Audio tag blocklist
   - Remove video with the tag **original** from the media tab.
   - Remove audio with the tag **nospeech** from the media tab.