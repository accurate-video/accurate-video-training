export default {
  backendUrl: "http://localhost:8080",
  licenseKey: "<YOUR LICENSE KEY>",
  //settingsUrl: "http://localhost/static/remote_settings.js",
  authentication: {
    enabled: false,
  },
  timeline: {
    waveforms: {
      active: "vidispine",
      vidispine: {
        tag: "original",
        shape: null,
        sampleMin: -43,
        sampleMax: 0,
      },
    },
  },
  assetStatus: {
    statusMetadataFieldName: "asset_status",
    commentMetadataFieldName: "asset_status_comment",
    statusSetByMetadataFieldName: "asset_status_set_by",
    statuses: [
      {
        key: "in_progress",
        labels: {
          status: "In progress",
        },
        color: "var(--AP-FOREGROUND-2)",
      },
      {
        key: "pending",
        labels: {
          status: "Pending",
        },
        color: "var(--AP-FOREGROUND-2)",
        hidden: true,
      },
      {
        key: "approved",
        type: "approved",
        labels: {
          status: "Approved",
          assign: "Approve",
        },
        color: "var(--AP-SUCCESS)",
      },
      {
        key: "rejected",
        labels: {
          status: "Rejected",
          assign: "Reject",
        },
        color: "var(--AP-ERROR)",
        allowComment: true,
      },
    ],
  },
  features: {
    assetStatus: true
  },
  apps: {
    validate: {
      markerTableDefinitions: [
        {
          id: "name",
          label: "Name",
          enabled: true,
          scope: "#/metadata/name",
          format: "STRING",
        },
        {
          id: "description",
          label: "Description",
          enabled: true,
          scope: "#/metadata/description",
          format: "STRING",
        },
        {
          id: "start",
          label: "Start",
          enabled: true,
          scope: "#/start",
          format: "TIMECODE",
        },
        {
          id: "end",
          label: "End",
          enabled: true,
          scope: "#/end",
          format: "TIMECODE",
        },
        {
          id: "duration",
          label: "Duration",
          enabled: true,
          scope: "#/duration",
          format: "TIMECODE",
        },
        {
          id: "group",
          label: "Group",
          enabled: true,
          scope: "#/group",
          format: "STRING",
        },
        {
          id: "track",
          label: "Track",
          enabled: true,
          scope: "#/track",
          format: "STRING",
        },
      ],
      features: {
        setLanguageOnFiles: true,
      }
    },
    poster: {
      includedMarkerGroups: ["Manual", "batonSegment"]
    }
  },
  forms: {
    defaultMarker: {
      schema: {
        type: "object",
        properties: {
          name: {
            type: "string",
          },
          description: {
            type: "string",
          },
        },
        required: ["name"],
      },
      uischema: {
        type: "VerticalLayout",
        elements: [
          {
            type: "Control",
            label: "Name",
            scope: "#/properties/name",
          },
          {
            type: "Control",
            label: "Description (optional)",
            scope: "#/properties/description",
            options: {
              format: "textarea",
            },
          },
        ],
      },
      defaultValues: {
        description: "",
      },
    },
    defaultAsset: {
      schema: {
        type: "object",
        properties: {
          title: {
            type: "string",
          },
          season: {
            type: "string",
            enum: ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10"],
          },
          episode: {
            type: "string",
            enum: ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10"],
          },
          genre: {
            type: "array",
            items: {
              enum: [
                "Action",
                "Adult",
                "Adventure",
                "Animation",
                "Biography",
                "Comedy",
                "Crime",
                "Documentary",
                "Drama",
                "Family",
                "Fantasy",
                "Film Noir",
                "Game Show",
                "History",
                "Horror",
                "Musical",
                "Music",
                "Mystery",
                "News",
                "Reality-TV",
                "Romance",
                "Sci-Fi",
                "Short",
                "Sport",
                "Talk-Show",
                "Thriller",
                "War",
                "Western",
              ],
            },
          },
          published: {
            type: "boolean",
          },
          description: {
            type: "string",
          },
        },
      },
      uischema: {
        type: "VerticalLayout",
        elements: [
          {
            type: "Control",
            label: "Title",
            scope: "#/properties/title",
          },
          {
            type: "HorizontalLayout",
            elements: [
              {
                type: "Control",
                label: "Season",
                scope: "#/properties/season",
              },
              {
                type: "Control",
                label: "Episode",
                scope: "#/properties/episode",
              },
            ],
          },
          {
            type: "Control",
            label: "Genre",
            scope: "#/properties/genre",
            options: {
              format: "multiselect",
            },
          },
          {
            type: "Control",
            label: "Published",
            scope: "#/properties/published",
            options: {
              toggle: true,
            },
          },
          {
            type: "Control",
            label: "Description",
            scope: "#/properties/description",
            options: {
              format: "textarea",
            },
          },
        ],
      },
      defaultValues: {
        published: "false",
      },
    },
  },
  metadataFields: [
    {
      id: "duration",
      key: "duration",
      label: "Duration",
      source: "properties",
      storedType: "timebase",
      displayType: "smpte",
    },
    {
      id: "resolution",
      key: "resolution",
      label: "Resolution",
      source: "properties",
    },
    {
      id: "frameRate",
      key: "frameRate",
      label: "Frame Rate",
      source: "properties",
    },
    {
      id: "aspectRatio",
      key: "aspectRatio",
      label: "Aspect Ratio",
      source: "properties",
    },
    {
      id: "bitrate",
      key: "bitrate",
      label: "Bitrate",
      source: "properties",
    },
    {
      id: "codec",
      key: "codec",
      label: "Codec",
      source: "properties",
    },
    {
      id: "channels",
      key: "channels",
      label: "Channels",
      source: "properties",
    },
    {
      id: "sampleRate",
      key: "sampleRate",
      label: "Sample Rate",
      source: "properties",
    },
    {
      id: "creationDate",
      key: "creationDate",
      label: "Created",
      source: "properties",
      displayType: "date-time",
      storedType: "utcDate",
    },
    {
      id: "updateDate",
      key: "updateDate",
      label: "Updated",
      source: "properties",
      displayType: "date-time",
      storedType: "utcDate",
    },
    {
      id: "assetType",
      key: "asset_type",
      label: "Asset type",
      source: "metadata"
    }
  ],
  metadataViews: [
    {
      active: true,
      id: "defaultMetadataView",
      name: "Default",
      description: "The default metadata view",
      fieldSets: {
        ["asset"]: {
          form: "defaultAsset",
          readOnlyFields: [
            {
              metadataFieldId: "creationDate",
              order: 1,
            },
            {
              metadataFieldId: "updateDate",
              order: 2,
            },
            {
              metadataFieldId: "assetType",
              order: 3,
            }
          ],
        },
        ["videoFile"]: {
          form: "defaultVideo",
        },
        ["videoStream"]: {
          readOnlyFields: [
            {
              metadataFieldId: "duration",
              order: 4,
            },
            {
              metadataFieldId: "frameRate",
              order: 5,
            },
            {
              metadataFieldId: "resolution",
              order: 6,
            },
            {
              metadataFieldId: "aspectRatio",
              order: 7,
            },
            {
              metadataFieldId: "codec",
              order: 8,
            },
            {
              metadataFieldId: "bitrate",
              order: 9,
            },
          ],
        },
        ["audioFile"]: {
          form: "defaultAudio",
        },
        ["audioStream"]: {
          readOnlyFields: [
            {
              metadataFieldId: "duration",
              order: 10,
            },
            {
              metadataFieldId: "channels",
              order: 11,
            },
            {
              metadataFieldId: "sampleRate",
              order: 12,
            },
            {
              metadataFieldId: "codec",
              order: 13,
            },
            {
              metadataFieldId: "bitrate",
              order: 14,
            },
          ],
        },
        ["subtitleFile"]: {
          form: "defaultSubtitle",
        },
      },
    },
  ],
  markers: {
    groups: [
      {
        match: (marker, track) =>
          track?.type === "Adbreak" ||
          marker?.type === "av:adbreak:track:break" ||
          marker?.type === "av:adbreak:track:cut",
        title: "Program",
        id: "Adbreak",
        alwaysShow: true,
        allowCreateTrack: false,
        trackType: "Adbreak",
        applicationFilters: [
          {
            application: "validate",
            workspace: "Adbreak",
          },
        ],
        rows: [
          {
            match: (marker) =>
              marker?.metadata.get("trackId") === "av:adbreak:track:break",
            track: "av:adbreak:track:break",
            title: "Start, End, Breaks",
            tooltip: (marker) => marker?.metadata.get("description"),
            tooltipFallback: "No description",
            order: 0,
            markerType: "av:adbreak:track:break",
          },
          {
            match: (marker) =>
              marker?.metadata.get("trackId") === "av:adbreak:track:cut",
            form: "cutForm",
            track: "av:adbreak:track:cut",
            title: "Cuts",
            tooltip: (marker) => marker?.metadata.get("description"),
            tooltipFallback: "No description",
            order: 1,
            markerType: "av:adbreak:track:cut",
            tag: {
              tag: "Subtracted from duration",
              type: "description",
            },
            markerStyle: {
              backgroundColor: "var(--AP-ERROR)",
            },
          },
        ],
      },
      {
        match: (marker) => marker?.type === "baton_error",
        title: "Baton Error",
        id: "batonSegment",
        readOnly: true,
        alwaysShow: false,
        rows: [
          {
            markerType: "Baton",
            track: "baton:track:error:info",
            match: ({ metadata }) => metadata.get("severity") === "Info",
            title: "Info",
            tooltip: ({ metadata }) => metadata.get("name"),
            order: 1,
          },
          {
            markerType: "Baton",
            track: "baton:track:error:warning",
            match: ({ metadata }) => metadata.get("severity") === "Warning",
            title: "Warning",
            tooltip: ({ metadata }) => metadata.get("name"),
            order: 2,
          },
          {
            markerType: "Baton",
            track: "baton:track:error:serious",
            match: ({ metadata }) => metadata.get("severity") === "Serious",
            title: "Serious",
            tooltip: ({ metadata }) => metadata.get("name"),
            order: 3,
          },
        ],
      },
      {
        match: (marker, track) =>
          marker?.type === "Manual" || track?.type === "Manual",
        title: "Marker Tracks",
        id: "Manual",
        alwaysShow: true,
        allowCreateTrack: true,
        trackType: "Manual",
        rows: [
          {
            match: (marker) =>
              marker?.metadata.get("trackId") === "av:track:video:issues",
            track: "av:track:video:issues",
            title: "Video issues",
            tooltip: (marker) => marker?.metadata.get("name"),
            order: 0,
            markerType: "Manual",
            names: [
              "Abrupt edit",
              "Aliasing",
              "Animation error",
              "Artifact",
              "Black frames",
              "Freeze frame",
              "Interlacing",
              "Jitter",
              "Letterboxing",
              "Posterization",
              "Shifted luminance",
            ],
            markerStyle: {
              backgroundColor: "var(--AP-PRIMARY)",
            },
          },
          {
            match: (marker) =>
              marker?.metadata.get("trackId") === "av:track:audio:issues",
            track: "av:track:audio:issues",
            title: "Audio issues",
            tooltip: (marker) => marker?.metadata.get("name"),
            order: 1,
            markerType: "Manual",
            names: [
              "Audio overlap",
              "Dialogue out of sync",
              "Distortion",
              "Dropout",
              "Glitch",
              "Loudness issue",
              "Missing dialogue",
              "Missing sound effect",
              "Out of pitch",
              "Wrong language on track",
            ],
            markerStyle: {
              backgroundColor: "var(--AP-AZURE)",
            },
          },
          {
            match: (marker) =>
              marker?.metadata.get("trackId") === "av:track:subtitle:issues",
            track: "av:track:subtitle:issues",
            title: "Subtitle issues",
            tooltip: (marker) => marker?.metadata.get("name"),
            order: 2,
            markerType: "Manual",
            names: [
              "Erratum",
              "Incorrect positioning",
              "Missing censorship",
              "Missing cue",
              "Mistimed cue",
              "Unnecessary censorship",
            ],
            markerStyle: {
              backgroundColor: "var(--AP-SUCCESS)",
            },
          },
          {
            match: (marker, track) =>
              !!marker?.metadata.get("trackId") || !!track,
            track: (marker, track) => track.id,
            title: (marker, track) =>
              track?.metadata.get("name"),
            tooltip: (marker) => marker?.metadata.get("name"),
            order: (marker, track) =>
              parseInt(track?.id, 10) + 3 ?? 4,
            markerType: "Manual",
            alwaysShow: false,
          },
        ],
      },
      {
        match: () => true, // Default
        id: (marker) => marker?.type,
        title: (marker) => marker?.type,
        alwaysHide: true,
        rows: [],
      },
    ],
    markersMetadataSettings: [
      {
        match: (type) => type === "baton_error",
        mappings: {
          name: "synopsis",
          description: undefined,
          trackId: undefined,
        },
      },
      {
        match: () => true, // Default
        mappings: {
          name: "name",
          description: "description",
          trackId: "track",
        },
      },
    ],
    tracksMetadataSettings: [
      {
        match: () => true, // Default
        mappings: {
          name: "name",
          description: "description",
        },
      },
    ],
  },
};
