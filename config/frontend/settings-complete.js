export default {
  backendUrl: "http://localhost:8080",
  licenseKey: "<YOUR LICENSE KEY>",
  //settingsUrl: "http://localhost/static/remote_settings.js",
  authentication: {
    enabled: false,
  },
  enabledApps: ["validate"],
  videoTagBlocklist: ["original"],
  audioTagBlocklist: ["nospeech"],
  timeline: {
    waveforms: {
      active: "vidispine",
      vidispine: {
        tag: "original",
        shape: null,
        sampleMin: -30,
        sampleMax: 15,
      },
    },
  },
  assetStatus: {
    statusMetadataFieldName: "asset_status",
    commentMetadataFieldName: "asset_status_comment",
    statusSetByMetadataFieldName: "asset_status_set_by",
    statuses: [
      {
        key: "in_progress",
        labels: {
          status: "In progress",
        },
        color: "var(--AP-FOREGROUND-2)",
      },
      {
        key: "pending",
        labels: {
          status: "Pending",
        },
        color: "var(--AP-FOREGROUND-2)",
        hidden: true,
      },
      {
        key: "semiapproved",
        type: "approved",
        labels: {
          status: "Semi approved",
          assign: "Semi approve",
        },
        form: "semiapprovedAssetStatusForm",
        color: "var(--AP-YELLOW)",
      },
      {
        key: "approved",
        type: "approved",
        labels: {
          status: "Approved",
          assign: "Approve",
        },
        color: "var(--AP-SUCCESS)",
      },
      {
        key: "rejected",
        labels: {
          status: "Rejected",
          assign: "Reject",
        },
        color: "var(--AP-ERROR)",
        allowComment: true,
      },
    ],
  },
  features: {
    assetStatus: true,
  },
  apps: {
    validate: {
      markerTableDefinitions: [
        {
          id: "name",
          label: "Name",
          enabled: true,
          scope: "#/metadata/name",
          format: "STRING",
        },
        {
          id: "actors",
          label: "Actors",
          enabled: true,
          scope: "#/metadata/actors",
          format: "ARRAY",
        },
        {
          id: "boolean",
          label: "Boolean",
          enabled: true,
          scope: "#/metadata/boolean",
          format: "BOOLEAN",
        },
        {
          id: "conditional",
          label: "Conditional",
          enabled: true,
          scope: "#/metadata/conditional",
          format: "STRING",
        },
        {
          id: "other",
          label: "Other",
          enabled: true,
          scope: "#/metadata/other",
          format: "STRING",
        },
        {
          id: "group",
          label: "Group",
          enabled: true,
          scope: "#/group",
          format: "STRING",
        },
        {
          id: "track",
          label: "Track",
          enabled: true,
          scope: "#/track",
          format: "STRING",
        },
        {
          id: "composite",
          label: "Composite",
          enabled: true,
          scope: (marker) => {
            let name = marker?.metadata?.get("name") ?? "";
            return (
              "nam: " + name + ", other: " + marker?.metadata?.get("other")
            );
          },
          valueMapper: (val) =>
            val?.includes("Choice 2")
              ? "You selected choice 2 (" + val + ")"
              : val,
          format: "STRING",
        },
      ],
      features: {
        setLanguageOnFiles: true,
      },
      enabledWorkspaces: ["General", "Audio"],
      videoFileDisplay: "%asset (%tag, %id)",
      audioFileDisplay: "%filename (%tag, %channels channels)",
      videoFileOrder: ["language"],
      audioFileOrder: ["tag"],
      
    },
    poster: {
      includedMarkerGroups: ["Manual", "batonSegment"]
    }
  },
  forms: {
    defaultMarker: {
      schema: {
        properties: {
          name: {
            type: "string",
          },
          description: {
            type: "string",
          },
          type: {
            type: "string",
            enum: ["Success", "Warning", "Error"],
          },
        },
        required: ["name"],
      },
      uischema: {
        type: "VerticalLayout",
        elements: [
          {
            type: "Control",
            label: "Name",
            scope: "#/properties/name",
          },
          {
            type: "Control",
            label: "Description (optional)",
            scope: "#/properties/description",
            options: {
              format: "textarea",
            },
          },
          {
            type: "Control",
            label: "Type",
            scope: "#/properties/type",
          },
        ],
      },
      defaultValues: {
        description: "",
      },
    },
    defaultAsset: {
      schema: {
        type: "object",
        properties: {
          title: {
            type: "string",
          },
          season: {
            type: "string",
            enum: ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10"],
          },
          episode: {
            type: "string",
            enum: ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10"],
          },
          genre: {
            type: "array",
            items: {
              enum: [
                "Action",
                "Adult",
                "Adventure",
                "Animation",
                "Biography",
                "Comedy",
                "Crime",
                "Documentary",
                "Drama",
                "Family",
                "Fantasy",
                "Film Noir",
                "Game Show",
                "History",
                "Horror",
                "Musical",
                "Music",
                "Mystery",
                "News",
                "Reality-TV",
                "Romance",
                "Sci-Fi",
                "Short",
                "Sport",
                "Talk-Show",
                "Thriller",
                "War",
                "Western",
              ],
            },
          },
          published: {
            type: "boolean",
          },
          description: {
            type: "string",
          },
        },
      },
      uischema: {
        type: "VerticalLayout",
        elements: [
          {
            type: "Control",
            label: "Title",
            scope: "#/properties/title",
          },
          {
            type: "HorizontalLayout",
            elements: [
              {
                type: "Control",
                label: "Season",
                scope: "#/properties/season",
              },
              {
                type: "Control",
                label: "Episode",
                scope: "#/properties/episode",
              },
            ],
          },
          {
            type: "Control",
            label: "Genre",
            scope: "#/properties/genre",
            options: {
              format: "multiselect",
            },
          },
          {
            type: "Control",
            label: "Published",
            scope: "#/properties/published",
            options: {
              toggle: true,
            },
          },
          {
            type: "Control",
            label: "Description",
            scope: "#/properties/description",
            options: {
              format: "textarea",
            },
          },
        ],
      },
      defaultValues: {
        published: "false",
      },
    },
    semiapprovedAssetStatusForm: {
      schema: {
        type: "object",
        properties: {
          asset_status_metadata: {
            type: "boolean",
            default: null,
            enum: [null, true],
          },
          asset_status_markers: {
            type: "boolean",
            default: null,
            enum: [null, true],
          },
          asset_status_audio: {
            type: "boolean",
            default: null,
            enum: [null, true],
          },
          asset_status_video: {
            type: "boolean",
            default: null,
            enum: [null, true],
          },
          asset_status_comment: {
            type: "string",
          },
        },
        required: [
          "asset_status_metadata",
          "asset_status_markers",
          "asset_status_audio",
          "asset_status_video",
        ],
      },
      uischema: {
        type: "VerticalLayout",
        elements: [
          {
            type: "Control",
            label: "Metadata reviewed",
            scope: "#/properties/asset_status_metadata",
            options: {
              toggle: true,
            },
          },
          {
            type: "Control",
            label: "All markers checked",
            scope: "#/properties/asset_status_markers",
            options: {
              toggle: true,
            },
          },
          {
            type: "Control",
            label: "Audio reviewed",
            scope: "#/properties/asset_status_audio",
            options: {
              toggle: true,
            },
          },
          {
            type: "Control",
            label: "Video inspected",
            scope: "#/properties/asset_status_video",
            options: {
              toggle: true,
            },
          },
          {
            type: "Control",
            label: "Additional comments",
            scope: "#/properties/asset_status_comment",
            options: {
              format: "textarea",
            },
          },
        ],
      },
    },
    newForm: {
      schema: {
        type: "object",
        properties: {
          name: {
            type: "string",
          },
          boolean: {
            type: "boolean",
          },
          conditional: {
            type: "string",
          },
          other: {
            type: "string",
            enum: ["Choice 1", "Choice 2"],
          },
          actors: {
            $ref: "http://localhost/static/actors-field.json",
          },
        },
      },
      uischema: {
        type: "VerticalLayout",
        elements: [
          {
            type: "Control",
            label: "Name",
            scope: "#/properties/name",
          },
          {
            type: "Control",
            label: "Actors",
            scope: "#/properties/actors",
            options: {
              format: "multiselect",
            },
          },
          {
            type: "Control",
            label: "Boolean",
            scope: "#/properties/boolean",
          },
          {
            type: "Group",
            label: "Group of fields",
            elements: [
              {
                type: "HorizontalLayout",
                elements: [
                  {
                    type: "Control",
                    label: "Only enabled if boolean field = true",
                    scope: "#/properties/conditional",
                    rule: {
                      effect: "ENABLE",
                      condition: {
                        scope: "#/properties/boolean",
                        schema: {
                          const: true,
                        },
                      },
                    },
                  },
                  {
                    type: "Control",
                    label: "Other",
                    scope: "#/properties/other",
                  },
                ],
              },
            ],
          },
        ],
      },
    },
  },
  metadataFields: [
    {
      id: "duration",
      key: "duration",
      label: "Duration",
      source: "properties",
      storedType: "timebase",
      displayType: "smpte",
    },
    {
      id: "resolution",
      key: "resolution",
      label: "Resolution",
      source: "properties",
    },
    {
      id: "frameRate",
      key: "frameRate",
      label: "Frame Rate",
      source: "properties",
    },
    {
      id: "aspectRatio",
      key: "aspectRatio",
      label: "Aspect Ratio",
      source: "properties",
    },
    {
      id: "bitrate",
      key: "bitrate",
      label: "Bitrate",
      source: "properties",
    },
    {
      id: "codec",
      key: "codec",
      label: "Codec",
      source: "properties",
    },
    {
      id: "channels",
      key: "channels",
      label: "Channels",
      source: "properties",
    },
    {
      id: "sampleRate",
      key: "sampleRate",
      label: "Sample Rate",
      source: "properties",
    },
    {
      id: "creationDate",
      key: "creationDate",
      label: "Created",
      source: "properties",
      displayType: "date-time",
      storedType: "utcDate",
    },
    {
      id: "updateDate",
      key: "updateDate",
      label: "Updated",
      source: "properties",
      displayType: "date-time",
      storedType: "utcDate",
    },
    {
      id: "assetType",
      key: "asset_type",
      label: "Asset type",
      source: "metadata",
      expectedMetadata: [
        {
          id: "expectedAssetType",
          key: "asset_type_expected",
          source: "metadata",
          comparison: {
            type: "boolean",
            key: "asset_type_expected_result",
            source: "metadata"
          }
        }
      ]
    },
    {
      id: "director",
      key: "director",
      label: "Director",
      source: "metadata",
    },
    {
      id: "releaseDate",
      key: "release_date",
      label: "Release date",
      source: "metadata",
      displayType: "date",
      storedType: "utcDate",
    },
  ],
  metadataViews: [
    {
      active: false,
      id: "defaultMetadataView",
      name: "Default",
      description: "The default metadata view",
      fieldSets: {
        ["asset"]: {
          form: "defaultAsset",
          readOnlyFields: [
            {
              metadataFieldId: "creationDate",
              order: 1,
            },
            {
              metadataFieldId: "updateDate",
              order: 2,
            },
            {
              metadataFieldId: "assetType",
              expectedMetadataFieldId: "expectedAssetType",
              order: 3,
            },
            {
              metadataFieldId: "director",
              order: 4,
            },
            {
              metadataFieldId: "releaseDate",
              order: 5,
            }
          ],
        },
        ["videoFile"]: {
          form: "defaultVideo",
        },
        ["videoStream"]: {
          readOnlyFields: [
            {
              metadataFieldId: "duration",
              order: 4,
            },
            {
              metadataFieldId: "frameRate",
              order: 5,
            },
            {
              metadataFieldId: "resolution",
              order: 6,
            },
            {
              metadataFieldId: "aspectRatio",
              order: 7,
            },
            {
              metadataFieldId: "codec",
              order: 8,
            },
            {
              metadataFieldId: "bitrate",
              order: 9,
            },
          ],
        },
        ["audioFile"]: {
          form: "defaultAudio",
        },
        ["audioStream"]: {
          readOnlyFields: [
            {
              metadataFieldId: "duration",
              order: 10,
            },
            {
              metadataFieldId: "channels",
              order: 11,
            },
            {
              metadataFieldId: "sampleRate",
              order: 12,
            },
            {
              metadataFieldId: "codec",
              order: 13,
            },
            {
              metadataFieldId: "bitrate",
              order: 14,
            },
          ],
        },
        ["subtitleFile"]: {
          form: "defaultSubtitle",
        },
      },
    },
    {
      active: true,
      id: "newMetadataView",
      name: "New",
      description: "The new metadata view",
      fieldSets: {
        ["asset"]: {
          form: "newForm",
          readOnlyFields: [
            {
              metadataFieldId: "director",
              order: 1,
            },
            {
              metadataFieldId: "releaseDate",
              order: 2,
            },
            {
              metadataFieldId: "assetType",
              expectedMetadataFieldId: "expectedAssetType",
              order: 3,
            }
          ],
        },
      },
    },
  ],
  markers: {
    groups: [
      {
        match: (marker, track) =>
          track?.type === "Adbreak" ||
          marker?.type === "av:adbreak:track:break" ||
          marker?.type === "av:adbreak:track:cut",
        title: "Program",
        id: "Adbreak",
        alwaysShow: true,
        allowCreateTrack: false,
        trackType: "Adbreak",
        applicationFilters: [
          {
            application: "validate",
            workspace: "Adbreak",
          },
        ],
        rows: [
          {
            match: (marker) =>
              marker?.metadata.get("trackId") === "av:adbreak:track:break",
            track: "av:adbreak:track:break",
            title: "Start, End, Breaks",
            tooltip: (marker) => marker?.metadata.get("description"),
            tooltipFallback: "No description",
            order: 0,
            markerType: "av:adbreak:track:break",
          },
          {
            match: (marker) =>
              marker?.metadata.get("trackId") === "av:adbreak:track:cut",
            form: "cutForm",
            track: "av:adbreak:track:cut",
            title: "Cuts",
            tooltip: (marker) => marker?.metadata.get("description"),
            tooltipFallback: "No description",
            order: 1,
            markerType: "av:adbreak:track:cut",
            tag: {
              tag: "Subtracted from duration",
              type: "description",
            },
            markerStyle: {
              backgroundColor: "var(--AP-ERROR)",
            },
          },
        ],
      },
      {
        match: (marker) => marker?.type === "baton_error",
        title: "Baton Error",
        id: "batonSegment",
        readOnly: false,
        alwaysShow: false,
        rows: [
          {
            markerType: "Baton",
            track: "baton:track:error:info",
            match: ({ metadata }) => metadata.get("severity") === "Info",
            title: "Info",
            tooltip: ({ metadata }) => metadata.get("name"),
            order: 1,
          },
          {
            markerType: "Baton",
            track: "baton:track:error:warning",
            match: ({ metadata }) => metadata.get("severity") === "Warning",
            title: "Warning",
            tooltip: ({ metadata }) => metadata.get("name"),
            order: 2,
          },
          {
            markerType: "Baton",
            track: "baton:track:error:serious",
            match: ({ metadata }) => metadata.get("severity") === "Serious",
            title: "Serious",
            tooltip: ({ metadata }) => metadata.get("name"),
            order: 3,
          },
        ],
      },
      {
        match: (marker, track) =>
          marker?.type === "Manual" || track?.type === "Manual",
        title: "Marker Tracks",
        id: "Manual",
        alwaysShow: true,
        allowCreateTrack: true,
        trackType: "Manual",
        rows: [
          {
            match: (marker) =>
              marker?.metadata.get("trackId") === "av:track:video:issues",
            track: "av:track:video:issues",
            title: "Video issues",
            tooltip: (marker) => marker?.metadata.get("name"),
            order: 0,
            markerType: "Manual",
            names: [
              "Abrupt edit",
              "Aliasing",
              "Animation error",
              "Artifact",
              "Black frames",
              "Freeze frame",
              "Interlacing",
              "Jitter",
              "Letterboxing",
              "Posterization",
              "Shifted luminance",
            ],
            markerStyle: {
              backgroundColor: "var(--AP-PRIMARY)",
            },
          },
          {
            match: (marker) =>
              marker?.metadata.get("trackId") === "av:track:audio:issues",
            track: "av:track:audio:issues",
            title: "Audio issues",
            tooltip: (marker) => marker?.metadata.get("name"),
            order: 1,
            markerType: "Manual",
            names: [
              "Audio overlap",
              "Dialogue out of sync",
              "Distortion",
              "Dropout",
              "Glitch",
              "Loudness issue",
              "Missing dialogue",
              "Missing sound effect",
              "Out of pitch",
              "Wrong language on track",
            ],
            markerStyle: {
              backgroundColor: "var(--AP-AZURE)",
            },
          },
          {
            match: (marker) =>
              marker?.metadata.get("trackId") === "av:track:subtitle:issues",
            track: "av:track:subtitle:issues",
            title: "Subtitle issues",
            tooltip: (marker) => marker?.metadata.get("name"),
            order: 2,
            markerType: "Manual",
            names: [
              "Erratum",
              "Incorrect positioning",
              "Missing censorship",
              "Missing cue",
              "Mistimed cue",
              "Unnecessary censorship",
            ],
            markerStyle: {
              backgroundColor: "var(--AP-SUCCESS)",
            },
          },
          {
            match: (marker, track) =>
              !!marker?.metadata.get("trackId") || !!track,
            track: (marker, track) => track.id,
            title: (marker, track) => track?.metadata.get("name"),
            tooltip: (marker) => marker?.metadata.get("name"),
            order: (marker, track) => parseInt(track?.id, 10) + 3 ?? 4,
            markerType: "Manual",
            alwaysShow: false,
          },
        ],
      },
      {
        match: (marker, track) => marker?.type === "New marker" || track?.type === "New track",
        title: "New group",
        id: "new",
        alwaysShow: true,
        allowCreateTrack: true,
        trackType: "New track",
        rows: [
          {
            match: (marker, track) =>
              marker?.metadata.get("trackId") === "new:track:one",
            track: "new:track:one",
            title: "New track 1",
            tooltip: (marker) => marker?.metadata.get("name"),
            markerType: "New marker",
            readOnly: false,
            markerStyle: {
              backgroundColor: "var(--AP-SUCCESS)",
            },
            form: "newForm",
          },
          {
            match: (marker, track) =>
              marker?.metadata.get("trackId") === "new:track:two",
            track: "new:track:two",
            title: "New track 2",
            tooltip: (marker) => marker?.metadata.get("description"),
            readOnly: true,
            markerType: "New marker",
            markerStyle: {
              backgroundColor: "var(--AP-WARNING)",
            },
          },
          {
            match: (marker, track) =>
              !!marker?.metadata.get("trackId") || !!track,
            track: (marker, track) => track?.id,
            title: (marker, track) => track?.metadata.get("name"),
            tooltip: (marker) => marker?.metadata.get("name"),
            order: (marker, track) => parseInt(track?.id, 10) + 3 ?? 4,
            markerType: "New marker",
            alwaysShow: false,
          },
        ],
      },
      {
        match: (marker, track) =>
          marker?.type === "auto_generated" || track?.type === "Auto",
        title: "Auto generated",
        id: "auto",
        alwaysShow: false,
        allowCreateTrack: true,
        trackType: "Auto",
        rows: [
          {
            match: (marker, track) => marker?.type === "auto_generated",
            track: (marker, track) =>
              marker?.metadata.get("trackId") || marker?.metadata.get("row"),
            title: (marker, track) =>
              marker?.metadata.get("trackId") || marker?.metadata.get("row"),
            tooltip: (marker) => marker?.metadata.get("name"),
            alwaysShow: false,
            readOnly: true,
            markerType: "auto_generated",
            markerStyle: {
              backgroundColor: "var(--AP-ERROR)",
            },
          },
          {
            match: (marker, track) => !!track,
            track: (marker, track) => track?.id,
            title: (marker, track) => track?.metadata.get("name"),
            tooltip: (marker) => marker?.metadata.get("name"),
            markerType: "auto:manual",
          },
        ],
      },
      {
        match: () => true, // Default
        id: (marker) => marker?.type,
        title: (marker) => marker?.type,
        alwaysHide: true,
        rows: [],
      },
    ],
    markersMetadataSettings: [
      {
        match: (type) => type === "baton_error",
        mappings: {
          name: "synopsis",
          description: undefined,
          trackId: undefined,
        },
      },
      {
        match: () => true, // Default
        mappings: {
          name: "name",
          description: "description",
          trackId: "track",
        },
      },
    ],
    tracksMetadataSettings: [
      {
        match: () => true, // Default
        mappings: {
          name: "name",
          description: "description",
        },
      },
    ],
  },
  languages: [
    {
      code: "ar-SA",
      label: "Arabic (Saudi Arabia)",
    },
    {
      code: "bn-BD",
      label: "Bangla (Bangladesh)",
    },
    {
      code: "bn-IN",
      label: "Bangla (India)",
    },
    {
      code: "cs-CZ",
      label: "Czech (Czech Republic)",
    },
    {
      code: "da-DK",
      label: "Danish (Denmark)",
    },
    {
      code: "de-AT",
      label: "Austrian German",
    },
    {
      code: "de-CH",
      label: "Swiss German",
    },
    {
      code: "de-DE",
      label: "Standard German (as spoken in Germany)",
    },
    {
      code: "el-GR",
      label: "Modern Greek",
    },
    {
      code: "en-AU",
      label: "Australian English",
    },
    {
      code: "en-CA",
      label: "Canadian English",
    },
    {
      code: "en-GB",
      label: "British English",
    },
    {
      code: "en-IE",
      label: "Irish English",
    },
    {
      code: "en-IN",
      label: "Indian English",
    },
    {
      code: "en-NZ",
      label: "New Zealand English",
    },
    {
      code: "en-US",
      label: "US English",
    },
    {
      code: "es-ES",
      label: "Castilian Spanish (as spoken in Central-Northern Spain)",
    },
    {
      code: "fr-FR",
      label: "Standard French (especially in France)",
    },
    {
      code: "it-IT",
      label: "Standard Italian (as spoken in Italy)",
    },
  ],
};
