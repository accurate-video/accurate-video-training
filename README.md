# Accurate Video Training

This repository holds the foundation for what is needed to quickly get started with validate in preparation for training.


## Requirements

* A Accurate.Video license key

* Docker + docker compose
* Python 3
* Pipenv
## Get started

1. Login to docker
   - `docker login codemill-docker.jfrog.io -u <username> -p <password>`
2. Add licenseKey to `config/frontend/settings.js`
3. Run `docker compose up -d`
4. Initialize a test asset
   1. See [test_asset_populator](test_asset_populator/README.md)
5. Run through the [exercises](EXERCISES.md)

## Troubleshooting

### Problems with jobs not working
If you are having issues ingesting data after running the asset import, you can try to 
change the configuration in the runner-cluster.xml.

Enable multicast and disable tcp-ip instead:

```
<tcp-ip enabled="false" connection-timeout-seconds="123">
    <member-list>
        <member>ap-backend</member>
        <member>ap-runner</member>
    </member-list>
</tcp-ip>
<multicast enabled="true"/>
```