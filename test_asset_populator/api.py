import os
import requests
from requests.auth import HTTPBasicAuth

BASE_URL = os.environ.get('BASE_URL', 'http://localhost:8080')
AUTH_USERNAME = os.environ.get('AUTH_USERNAME', 'training')
AUTH_PASSWORD = os.environ.get('AUTH_PASSWORD', 'training')

auth = HTTPBasicAuth(username=AUTH_USERNAME, password=AUTH_PASSWORD)


def pretty_print_request(req):
    print('{}\n{}\r\n{}\r\n\r\n{}'.format(
        '-----------START-----------',
        req.method + ' ' + req.url,
        '\r\n'.join('{}: {}'.format(k, v) for k, v in req.headers.items()),
        req.body,
    ))


def request(method, url, *args, **kwargs):
    response = method(url=f'{BASE_URL}{url}', auth=auth, *args, **kwargs)
    response.raise_for_status()
    return response


def get(url, *args, **kwargs):
    return request(requests.get, url, *args, **kwargs)


def post(url, *args, **kwargs):
    return request(requests.post, url, *args, **kwargs)


def put(url, *args, **kwargs):
    return request(requests.put, url, *args, **kwargs)


def delete(url, *args, **kwargs):
    return request(requests.delete, url, *args, **kwargs)
