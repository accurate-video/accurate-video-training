#!/usr/bin/env python3

import json
import time
import click
from unipath import Path

from api import get, post, put, pretty_print_request


@click.command()
@click.argument('input', type=click.File('rb'))
def import_assets(input):
    try:
        create_storages(input)
    except Exception as e:
        click.echo("Request failed")
        pretty_print_request(e.request)
        click.echo(e)
        click.echo(e.response.json())


def create_storages(input):
    response_data = get('/storage').json()
    existing_storages = response_data.get('values', [])
    existing_storages_lookup = {x['uri']: x for x in existing_storages}
    storages = json.load(input)
    for storage_json in storages:
        storage_key = storage_json['uri']
        asset_files = storage_json.pop('assets')
        if storage_key in existing_storages_lookup:
            existing_storage = existing_storages_lookup.get(storage_key)
            click.echo(f'Skipping storage as it already exists.')
        else:
            click.echo(f'Creating storage... ', nl=False)
            existing_storage = post('/storage', json=storage_json).json()
            click.echo(f'done.')
        storage_id = existing_storage['id']
        click.echo(f'Storage id {storage_id}')
        for asset_file in asset_files:
            asset_file_path = Path(input.name).parent.child(asset_file)
            click.echo(f'Creating asset {asset_file_path}')
            create_asset(asset_file_path, storage_id)


def create_asset(asset_file_path, storage_id):
    with open(asset_file_path) as asset_file:
        input_data = json.load(asset_file)
        for file in input_data.get('files', []):
            file['storageId'] = storage_id
            file['type'] = 'AUTO'
        click.echo(f'Asset data: {input_data}')
        timespans = input_data.pop('timespans', [])
        # Create asset
        click.echo(f'Creating asset... ', nl=False)
        asset = post('/asset', json=input_data).json()
        click.echo(f'done.')
        asset_id = asset['id']
        click.echo(f'Asset: {asset_id}')
        # Create markers from markers list
        for timespan in timespans:
            click.echo(f'Load timespan: {timespan}')
            timespan_file_path = Path(asset_file.name).parent.child(timespan)
            create_timespans(timespan_file_path, asset_id)


def create_timespans(timespan_file_path, asset_id):
    with open(timespan_file_path) as timespan_file:
        timespans_json = json.load(timespan_file)
        click.echo(f'Creating timespans... ', nl=False)
        post(f'/asset/{asset_id}/timespan/bulk', json=timespans_json)
        click.echo(f'done.')


if __name__ == '__main__':
    import_assets()
