#!/usr/bin/env python3

import click

from import_assets import create_timespans


@click.command()
@click.argument('timespans_file_path')
@click.argument('asset_id')
def import_timespans(timespans_file_path, asset_id):
    create_timespans(timespans_file_path, asset_id)


if __name__ == '__main__':
    import_timespans()
