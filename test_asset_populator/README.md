### Requirements

Pipenv: https://github.com/pypa/pipenv

### Run

1. Install the python environment with pipenv 

    ```
    pipenv install --python=python3
    ```

2. Activate the python environment with pipenv

    ```
    pipenv shell
    ```

3. Run the import script (in the activated python environment)

    ```
    ./import_assets.py fake_data/storages.json
    ```

4. Success


### Options

You can change the base url, and basic authentication by setting
BASE_URL, AUTH_USERNAME and AUTH_PASSWORD environment variables respectively.

i.e.

```
BASE_URL=https://anotherurl.com/ ./import_assets.py ../fake_data/storages.json
```
